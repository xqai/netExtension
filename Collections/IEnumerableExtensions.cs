﻿// ***********************************************************************
// Assembly         : XQ.Extension
// Author           : xq-notebook
// Created          : 01-26-2019
//
// Last Modified By : xq-notebook
// Last Modified On : 01-26-2019
// ***********************************************************************
// <copyright file="IEnumerableExtensions.cs" company="xiqiang">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace XQ.Extension
{
    /// <summary>
    /// IEnumerable 扩展类
    /// </summary>
    public static class IEnumerableExtend
    {

        /// <summary>
        /// 队列有数据判断
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this">The this.</param>
        /// <returns><c>true</c> if [is null or no data] [the specified this]; otherwise, <c>false</c>.</returns>
        public static bool HasData<T>(this IEnumerable<T> @this)
        {
            return !@this.IsNullOrNoData();
        }
        /// <summary>
        /// 队列无有数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this">The this.</param>
        /// <returns><c>true</c> if [is null or no data] [the specified this]; otherwise, <c>false</c>.</returns>
        public static bool IsNullOrNoData<T>(this IEnumerable<T> @this)
        {
            return @this == null || !@this.Any();
        }



        /// <summary>
        /// 复制一个新的集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this">The this.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        public static IEnumerable<T> CopyList<T>(this IEnumerable<T> @this) where T : class
        {
            if (@this == null)
                return null;
            if (@this.Count() == 0)
                return new T[0].ToList();

            var listr = new List<T>();
            @this.ToList().ForEach(o =>
            {
                listr.Add(SerializeHelper.DeepClone<T>(o));
            });
            return listr;
        }


        /// <summary>
        /// 复制一个新的集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this">The this.</param>
        /// <param name="fun">The fun.</param>
        /// <returns>IEnumerable&lt;T&gt;.</returns>
        public static IEnumerable<T> CopyList<T>(this IEnumerable<T> @this, Func<T, T> fun) where T : class
        {
            if (@this == null)
                return null;
            if (@this.Count() == 0)
                return new T[0].ToList();

            var listr = new List<T>();
            @this.ToList().ForEach(o =>
            {
                var model = SerializeHelper.DeepClone<T>(o);
                if (fun != null) model = fun.Invoke(model);
                listr.Add(model);
            });
            return listr;
        }

        /// <summary>
        /// ForEach
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this">The this.</param>
        /// <param name="act">The act.</param>
        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> @this, Action<T> act)
        {
            if (@this == null)
                return new List<T>();

            var ls = @this.ToList();
            ls.ForEach(o => act(o));
            return ls;
        }
    }
}
