﻿// ***********************************************************************
// Assembly         : XQ.Extension
// Author           : xq-notebook
// Created          : 01-26-2019
//
// Last Modified By : xq-notebook
// Last Modified On : 01-26-2019
// ***********************************************************************
// <copyright file="IListExtensions.cs" company="xiqiang">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace  XQ.Extension
{
    /// <summary>
    /// IList扩展类
    /// </summary>
    public static class IListExtensions
    {
       

        /// <summary>
        /// 转化一个DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="this">The this.</param>
        /// <returns>DataTable.</returns>
        public static DataTable ToDataTable<T>(this IList<T> @this)
        { 
            //创建属性的集合    
            var pList = new List<PropertyInfo>();
            //获得反射的入口  
            var type = typeof(T);
            var dt = new DataTable();
            //把所有的public属性加入到集合 并添加DataTable的列    
            Array.ForEach<PropertyInfo>(type.GetProperties(), p =>
            {
                pList.Add(p);
                try
                {
                    dt.Columns.Add(p.Name, p.PropertyType);
                }
                catch (Exception ex)
                { 
                    dt.Columns.Add(p.Name);
                } 
            });
            foreach (var item in @this)
            {
                //创建一个DataRow实例    
                DataRow row = dt.NewRow();
                //给row 赋值    
                pList.ForEach(p => row[p.Name] = p.GetValue(item, null));
                //加入到DataTable    
                dt.Rows.Add(row);
            }
            return dt;
        }
         
    }
}
