﻿// ***********************************************************************
// Assembly         : XQ.Extension
// Author           : xq-notebook
// Created          : 01-26-2019
//
// Last Modified By : xq-notebook
// Last Modified On : 01-26-2019
// ***********************************************************************
// <copyright file="HasTableExtensions.cs" company="xiqiang">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  XQ.Extension
{
    /// <summary>
    /// Class HasTableExtensions.
    /// </summary>
    public static class HasTableExtensions
    {
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="skey">The skey.</param>
        /// <returns>System.Object.</returns>
        public static object GetValue(this Hashtable @this, string skey)
        {
            if (@this == null || @this.Count <= 0)
                return null;
            foreach (string key in @this.Keys)
            {
                if (key == skey)
                {
                    return @this[key];
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the string value.
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="skey">The skey.</param>
        /// <returns>System.String.</returns>
        public static string GetStrValue(this Hashtable @this, string skey)
        {
            return @this.GetValue(skey).ToStrTrim();
        }
    }
}
