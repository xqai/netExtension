﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension
{
    /// <summary>
    /// 辅助类
    /// </summary>
    public class XString
    {
        /// <summary>
        /// 是否为空
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        internal static bool IsNullOrWhiteSpace(string @this)
        {
            return @this == null || @this == string.Empty || @this.Trim() == string.Empty;
        }
    }
}
