﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension
{
    /// <summary>
    /// Int扩展类
    /// </summary>
    public static class IntExtensions
    {
        private static readonly string[][] _RomanMapping = new[]
        {
            new[] {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"},
            new[] {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"},
            new[] {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"},
            new[] {"", "M", "MMM", "MMM"}
        };

        /// <summary>
        /// 16进制转10进制
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int FromHex(string str)
        {
            if (XString.IsNullOrWhiteSpace(str))
                return 0;
            str = str.Trim();
            if (str.StartsWith("0x", StringComparison.InvariantCultureIgnoreCase))
            {
                str = str.Substring(2);
                if (str.Length == 0)
                    return 0;
            }
            if (str.Length > 8)
                str = str.Substring(0, 8);
            try
            {
                // between 1 and 8 hex decimals
                return Convert.ToInt32(str, 16);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// To the hexadecimal.
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="useLeadingZeroes">if set to <c>true</c> [use leading zeroes].</param>
        /// <returns>System.String.</returns>
        public static string ToHex(this int @this, bool useLeadingZeroes = false)
        {
            if (useLeadingZeroes)
                return @this.ToString("x8");
            return @this.ToString("x");
        } 

    }
}
