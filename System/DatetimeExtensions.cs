﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension
{
    /// <summary>
    /// DateTime的扩展类
    /// </summary>
    public static class DatetimeExten
    {
        /// <summary>
        /// 日期转换(yyyy-MM-dd）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToDateStr(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 日期转换(yyyy-MM-dd）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToStringyyyy_MM_dd(this DateTime @this)
        {
            return @this.ToDateStr();
        }


        /// <summary>
        /// 日期转换(HH:mm:ss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToTimeStr(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("HH:mm:ss");
        }


        /// <summary>
        /// 日期转换(HH:mm:ss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToStringHH_mm_ss(this DateTime @this)
        {
            return @this.ToTimeStr();
        }


        /// <summary>
        /// 日期转换(yyyy-MM-dd HH:mm:ss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToLongDateStr(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /// <summary>
        /// 日期转换(yyyy-MM-dd HH:mm:ss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToStringyyyy_MM_dd_HH_mm_ss(this DateTime @this)
        {
            return @this.ToLongDateStr();
        }

        /// <summary>
        /// 日期转换(yyyyMMdd）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToDateStrNoF(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("yyyyMMdd");
        }





        /// <summary>
        /// 日期转换(yyyyMMdd）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToStringyyyyMMdd(this DateTime @this)
        {
            return @this.ToDateStrNoF();
        }


        /// <summary>
        /// 日期转换(HHmmss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToTimeStrNoF(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("HHmmss");
        }


        /// <summary>
        /// 日期转换(HHmmss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToStringHHmmss(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("HHmmss");
        }

        /// <summary>
        /// 日期转换(yyyyMMddHHmmss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToLongDateStrNoF(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("yyyyMMddHHmmss");
        }

        /// <summary>
        /// 日期转换(yyyyMMddHHmmss）格式
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string ToStringyyyyMMddHHmmss(this DateTime @this)
        {
            if (@this == null)
                new ArgumentNullException("日期对象为空");
            return @this.ToString("yyyyMMddHHmmss");
        }
    }
}
