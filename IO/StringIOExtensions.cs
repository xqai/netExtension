﻿// ***********************************************************************
// Assembly         : XQ.Extension
// Author           : xq-notebook
// Created          : 01-26-2019
//
// Last Modified By : xq-notebook
// Last Modified On : 01-26-2019
// ***********************************************************************
// <copyright file="StringIOExtensions.cs" company="xiqiang">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace XQ.Extension 
{
    /// <summary>
    /// Class StringIOExtensions.
    /// </summary>
    public static partial  class StringIOExtensions
    {
        #region IO
        /// <summary>
        /// 组合两个文件路径
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="path2">The path2.</param>
        /// <returns>System.String.</returns>
        public static string PathCombine(this string @this, string path2)
        {
            var npath = Path.Combine(@this, path2);
            return npath;
        }

        /// <summary>
        /// 获取当前字符串路径的目录信息
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns>System.String.</returns>
        public static string PathDirName(this string @this)
        {
            var dirName = Path.GetDirectoryName(@this);
            return dirName;
        }


        /// <summary>
        /// 获取当前字符串路径的扩展名
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns>System.String.</returns>
        public static string PathExtension(this string @this)
        {
            var name = Path.GetExtension(@this);
            return name;
        }

        /// <summary>
        /// 获取当前字符串路径的文件名和扩展名。
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns>System.String.</returns>
        public static string PathFileName(this string @this)
        {
            var name = Path.GetFileName(@this);
            return name;
        }

        /// <summary>
        /// 获取当前字符串路径的不包含扩展名的文件名
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns>System.String.</returns>
        public static string PathFileNameWithoutExtension(this string @this)
        {
            var name = Path.GetFileNameWithoutExtension(@this);
            return name;
        }

        /// <summary>
        /// 验证当前字符串路径是否有文件
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool PathHasFile(this string @this)
        {
            return File.Exists(@this);
        }


        /// <summary>
        /// 验证当前字符串路径是否有目录
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool PathHasDirectory(this string @this)
        {
            return Directory.Exists(@this);
        }

        /// <summary>
        /// 当前字符串路径创建新的目录，可能报错
        /// </summary>
        /// <param name="this">The this.</param>
        public static void PathNewDirectory(this string @this)
        {
            if (!@this.PathHasDirectory())
                Directory.CreateDirectory(@this);
        }

        /// <summary>
        /// 读取当前字符串路径文件内容到字符串中
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns>System.String.</returns>
        public static string PathLoadFileToStr(this string @this)
        {
            var val = string.Empty;
            if (!@this.PathHasFile())
                return val;
            using (var fs = new FileStream(@this, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var sr = new StreamReader(fs))
                {
                    val = sr.ReadToEnd();
                }
            }
            return val;
        }


        /// <summary>
        /// 读取当前字符串文件到数组中
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns>System.Byte[].</returns>
        /// <exception cref="Exception">未找到当前文件</exception>
        public static byte[] PathLoadFileToByte(this string @this)
        {

            if (!@this.PathHasFile())
                throw new Exception("未找到当前文件");
            byte[] array = null;
            using (var fs = new FileStream(@this, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                //获取文件大小
                long size = fs.Length;
                array = new byte[size];
                //将文件读到byte数组中
                fs.Read(array, 0, array.Length);
            }
            return array;
        }



        /// <summary>
        /// 读取当前字符串文件到数组中
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns>System.String.</returns>
        public static string PathLoadFileToByte64(this string @this)
        {
            var array = @this.PathLoadFileToByte();
            string result = Convert.ToBase64String(array);
            return result;
        }
        #endregion
    }
}
