﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace XQ.Extension
{
    /// <summary>
    /// 字符串扩展类
    /// </summary>
    public static partial class StringExtensions
    {
        /// <summary>
        /// 拆分字符串根据指定字符串(字符串会拆分单支付）
        /// </summary>
        /// <param name="this"></param>
        /// <param name="splitstr"></param>
        /// <returns></returns>
        public static string[] Split(this string @this, string splitstr)
        {
            return @this.Split(splitstr.ToCharArray());
        }


        /// <summary>
        /// 去除重复并且转大写
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string TrimUpper(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
            {
                return string.Empty;
            }
            return @this.Trim().ToUpper();
        }

        /// <summary>
        /// 去除重复并且转小写
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string TrimLower(this string @this)
        {
            if (@this.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }
            return @this.Trim().ToLower();
        }

        /// <summary>
        /// 去除重复Null不报错转空白字符串
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string XTrim(this string @this)
        {
            if (@this.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }
            return @this.Trim();
        }

        /// <summary>
        /// 是否Null或Empty
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string @this)
        {
            return string.IsNullOrEmpty(@this);
        }

        /// <summary>
        /// 是否Null或Empty
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this string @this)
        {
            return XString.IsNullOrWhiteSpace(@this);
        }

        /// <summary>
        /// 是否有数据
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool HasValue(this string @this)
        {
            return !@this.IsNullOrWhiteSpace();
        }
        /// <summary>
        /// 字符串是否是数字
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsInt(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return false;
            var i = 0;
            return int.TryParse(@this, out i);
        }

        /// <summary>
        /// 转Int
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static int ToInt(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return 0;

            int i = 0;
            int.TryParse(@this, out i);
            return i;
        }

        /// <summary>
        /// 是否Bool
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsBool(this string @this)
        {
            if (@this == "1" || @this.XTrim().ToUpper() == "TRUE")
                return true;
            bool flag = false;
            return bool.TryParse(@this, out flag);
        }


        /// <summary>
        /// 转Int
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool ToBool(this string @this)
        {
            if (@this.IsNullOrWhiteSpace())
                return false;
            if (@this == "1" || @this.XTrim().ToUpper() == "TRUE")
                return true;
            return false;
        }


        /// <summary>
        /// 是否Double
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsDouble(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return false;
            double flag = 0d;
            return double.TryParse(@this, out flag);
        }


        /// <summary>
        /// 转ToDouble
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static double ToDouble(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return 0;
            double i = 0;
            double.TryParse(@this, out i);
            return i;

        }


        /// <summary>
        /// 是否Decimal
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsDecimal(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return false;
            decimal flag = 0;
            return decimal.TryParse(@this, out flag);
        }

        /// <summary>
        /// 转Int
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static decimal ToDecimal(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return 0;
            decimal i = 0m;
            decimal.TryParse(@this, out i);
            return i;

        }


        /// <summary>
        /// 是否DateTime
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool IsDateTime(this string @this)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return false;
            var dt = DateTime.Now;
            return DateTime.TryParse(@this, out dt);
        }


        /// <summary>
        /// String转DateTime
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string @this)
        {
            var dt = DateTime.Now;
            if (DateTime.TryParse(@this, out dt))
            {
                return dt;
            }
            throw new Exception(@this + "转换日期格式错误");

        }

        /// <summary>
        /// String不是Null获取空字符串
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool NoNullOrWhiteSpace(this string @this)
        {
            return !@this.IsNullOrWhiteSpace();
        }

        /// <summary>
        /// String转DateTime
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static DateTime ToDateTimeErrNow(this string @this)
        {
            var dt = DateTime.Now;
            if (DateTime.TryParse(@this, out dt))
            {
                return dt;
            }
            else
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// String(19900101010101)转DateTime(1990-01-01 01:01:01)
        /// </summary>
        /// <param name="@this">日期格式字符串</param>
        /// <returns></returns>
        public static DateTime ToDateTime2(this string @this)
        {
            if (@this.Length > 0)
            {
                @this = @this.Substring(0, 4);
                @this += "-" + @this.Substring(5, 2);
                @this += "-" + @this.Substring(7, 2);
                @this += " " + @this.Substring(9, 2);
                @this += ":" + @this.Substring(11, 2);
                @this += ":" + @this.Substring(@this.Length - 2, 2);
                return @this.ToDateTime();
            }
            return DateTime.Now;

        }

        /// <summary>
        /// 单字符替换
        /// </summary>
        /// <param name="this"></param>
        /// <param name="oldstr"></param>
        /// <param name="newstr"></param>
        /// <returns></returns>
        public static string Replace(this string @this, string oldstr, string newstr)
        {
            if (oldstr.Length > 0 || newstr.Length > 0)
            {
                throw new Exception("字符长度不能大于1");
            }
            var c1 = oldstr[0];
            var c2 = newstr[0];
            return @this.Replace(c1, c2);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="this"></param>
        /// <param name="endstr"></param>
        /// <returns></returns>
        public static string TrimEnd(this string @this, string endstr)
        {
            return @this.TrimEnd(endstr.ToCharArray());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="this"></param>
        /// <param name="endstr"></param>
        /// <returns></returns>
        public static string TrimStart(this string @this, string endstr)
        {
            return @this.TrimStart(endstr.ToCharArray());
        }


        /// <summary>
        /// 转换16进值
        /// </summary>
        /// <param name="this"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string ToSixIntStr(this string @this, int length = 4)
        {
            if (XString.IsNullOrWhiteSpace(@this))
                return string.Empty;
            if (@this.IndexOf("0x") == -1)
            {
                var value = 0;
                if (!int.TryParse(@this, out value))
                {
                    return string.Empty;
                }
                @this = value.ToHex(true);
                if (@this.Length >= length)//长度大于
                    @this = "0x" + @this.Substring(@this.Length - length, length);
                else
                {
                    //不足
                    var cleng = length - @this.Length;
                    var temp = "0x";
                    for (int i = 0; i < cleng; i++)
                    {
                        temp += "0";
                    }
                    @this = temp + @this;
                }

            }
            return @this;
        }





        /// <summary>
        /// Null转换空字符串
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string NullToEmpty(this string @this)
        {
            if (null == @this)
                return string.Empty;
            return @this;
        }

        /// <summary>
        /// 取左侧的制定长度
        /// 如果null返回空字符串
        /// 如果字符串长度小于制定长度，返回字符串
        /// 如果字符串长度大于返回左侧制定长度
        /// </summary>
        /// <param name="this">字符串</param>
        /// <param name="length">截取长度</param>
        /// <returns></returns>
        public static string Left(this string @this, int length)
        {
            if (@this.IsNullOrWhiteSpace())
                return string.Empty;
            if (@this.Length < length)
                return @this;
            return @this.Substring(0, length);
        }


        /// <summary>
        /// 取右侧的制定长度
        /// 如果null返回空字符串
        /// 如果字符串长度小于制定长度，返回字符串
        /// 如果字符串长度大于返回右侧制定长度
        /// </summary>
        /// <param name="this">字符串</param>
        /// <param name="length">截取长度</param>
        /// <returns></returns>
        public static string Right(this string @this, int length)
        {
            if (@this.IsNullOrWhiteSpace())
                return string.Empty;
            if (@this.Length < length)
                return @this;
            return @this.Substring(@this.Length - length, length);
        }



    }
}
