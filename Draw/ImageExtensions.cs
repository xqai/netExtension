﻿// ***********************************************************************
// Assembly         : XQ.Extension
// Author           : xq-notebook
// Created          : 01-26-2019
//
// Last Modified By : xq-notebook
// Last Modified On : 06-23-2019
// ***********************************************************************
// <copyright file="ImageExtensions.cs" company="xiqiang">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace XQ.Extension
{
    /// <summary>
    /// Class ImageExtensions.
    /// </summary>
    public static class ImageExtensions
    {

        /// <summary>
        /// Cuts the specified width.
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>Image.</returns>
        public static Image Cut(this Image @this, int width, int height, int x, int y)
        {
            var r = new Bitmap(width, height);
            var destinationRectange = new Rectangle(0, 0, width, height);
            var sourceRectangle = new Rectangle(x, y, width, height);

            using (Graphics g = Graphics.FromImage(r))
            {
                g.DrawImage(@this, destinationRectange, sourceRectangle, GraphicsUnit.Pixel);
            }

            return r;
        }


        /// <summary>
        /// 放大缩小图片尺寸
        /// </summary>
        /// <param name="bit">The bit.</param>
        /// <param name="dHeight">Height of the d.</param>
        /// <param name="dWidth">Width of the d.</param>
        /// <param name="format">The format.</param>
        /// <returns>Image.</returns>
        public static Image PicSized(this Image bit, int dHeight, int dWidth, ImageFormat format)
        {
            try
            {
                Image originBmp = bit;
                int w = dWidth;
                int h = dHeight;
                Bitmap resizedBmp = new Bitmap(w, h);
                Graphics g = Graphics.FromImage(resizedBmp);
                //设置高质量插值法   
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                //设置高质量,低速度呈现平滑程度   
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                //消除锯齿 
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.DrawImage(originBmp, new Rectangle(0, 0, w, h), new Rectangle(0, 0, originBmp.Width, originBmp.Height), GraphicsUnit.Pixel);
                g.Dispose();
                var oldImag = bit;
                originBmp = resizedBmp;
                oldImag.Dispose();
                return originBmp;
            }
            catch (Exception ex)
            {
                return bit;
            }

        }

        /// <summary>
        /// 主屏幕截图
        /// </summary>
        /// <returns></returns>
        public static Image PrimaryScreenToBit()
        {
            var height = Screen.PrimaryScreen.Bounds.Height;
            var width = Screen.PrimaryScreen.Bounds.Width;
            var bit = new Bitmap(width, height);
            using (var g = Graphics.FromImage(bit))
            {
                g.CopyFromScreen(0, 0, 0, 0, new Size(width, height));
            }
            return bit;
        }

        /// <summary>
        /// byte[]转btnmap
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>Image.</returns>
        public static Image BytesToBitmap(byte[] bytes)
        {
            using (var ms = new MemoryStream(bytes))
            {
                var bitmap = new System.Drawing.Bitmap(ms);
                return bitmap;
            }
        }


        /// <summary>
        /// 图片转byte[]
        /// </summary>
        /// <param name="Bitmap">The bitmap.</param>
        /// <returns>System.Byte[].</returns>
        public static byte[] BitmapToBytes(this Image Bitmap)
        {
            using (var ms = new MemoryStream())
            {
                Bitmap.Save(ms, Bitmap.RawFormat);
                byte[] byteImage = new Byte[ms.Length];
                byteImage = ms.ToArray();
                return byteImage;
            }
        }
    }
}
