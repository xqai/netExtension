﻿// ***********************************************************************
// Assembly         : XQ.Extension
// Author           : xq-notebook
// Created          : 01-26-2019
//
// Last Modified By : xq-notebook
// Last Modified On : 01-26-2019
// ***********************************************************************
// <copyright file="DataSetExtensions.cs" company="xiqiang">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XQ.Extension
{
    /// <summary>
    /// DataSet扩展类
    /// </summary>
    public static class DataSetExtensions
    {
        /// <summary>
        /// 是否包含数据
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static bool HasData(this DataSet @this)
        {
            return !@this.IsNullOrNoData();
        }

        /// <summary>
        /// 返回数据集合的第一个表
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static DataTable FirstOrNull(this DataSet @this)
        {
            if (@this != null && @this.Tables.Count > 0)
                return @this.Tables[0];
            return null;
        }

        /// <summary>
        /// 是否Null或没有数据
        /// </summary>
        /// <param name="this">The this.</param>
        /// <returns><c>true</c> if [is null or no data] [the specified this]; otherwise, <c>false</c>.</returns>
        public static bool IsNullOrNoData(this DataSet @this)
        {
            if (@this == null)
                return true;
            if (@this.Tables.Count <= 0)
                return true;
            var flag = true;
            foreach (DataTable tab in @this.Tables)
            {
                if (tab.Rows.Count > 0)
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
    }
}
