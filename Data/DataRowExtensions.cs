﻿// ***********************************************************************
// Assembly         : XQ.Extension
// Author           : xq-notebook
// Created          : 01-26-2019
//
// Last Modified By : xq-notebook
// Last Modified On : 01-26-2019
// ***********************************************************************
// <copyright file="DataRowExtensions.cs" company="xiqiang">
//     Copyright ©  2019
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace XQ.Extension
{
    /// <summary>
    /// DataRow 扩展类
    /// </summary>
    public static class DataRowExtensions
    {
        /// <summary>
        /// 当前行第一列值
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static object FirstVal(this DataRow @this)
        {
            if (@this == null)
                return null;
            var obj = @this[0];
            return obj;

        }

        /// <summary>
        /// 获取一个列的值
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns>System.Object.</returns>
        public static object Item(this DataRow @this, string colName)
        {
            if (@this == null)
                return null;
            var dt = @this.Table;
            if (!dt.Columns.Contains(colName))
                return null;
            return @this[colName];
        }
        /// <summary>
        /// 获取一个列的值
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns>System.Object.</returns>
        public static object CValue(this DataRow @this, string colName)
        {
            if (@this == null)
                return null;
            var dt = @this.Table;
            if (!dt.Columns.Contains(colName))
                return null;
            return @this[colName];
        }

        /// <summary>
        /// 获取一行字段值转成String
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns>System.String.</returns>
        public static string CValueStr(this DataRow @this, string colName)
        {
            return @this.CValue(colName).ToStr();
        }


        /// <summary>
        /// 获取一行字段值转成String
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns>System.String.</returns>
        public static string CValueStrTrim(this DataRow @this, string colName)
        {
            return @this.CValue(colName).ToStrTrim();
        }

        /// <summary>
        /// 获取一行字段值转成Int
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns>System.Int32.</returns>
        public static int CValueInt(this DataRow @this, string colName)
        {
            return @this.CValueStr(colName).ToInt();
        }

        /// <summary>
        /// 获取一行字段值转成Double
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns>System.Double.</returns>
        public static double CValueDouble(this DataRow @this, string colName)
        {
            return @this.CValueStr(colName).ToDouble();
        }


        /// <summary>
        /// 获取一行字段值转成Decimal
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns>System.Decimal.</returns>
        public static decimal CValueDecimal(this DataRow @this, string colName)
        {
            return @this.CValueStr(colName).ToDecimal();
        }

        /// <summary>
        /// 获取一行字段值转成Bool
        /// </summary>
        /// <param name="this">The this.</param>
        /// <param name="colName">列名称</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        public static bool CValueBool(this DataRow @this, string colName)
        {
            return @this.CValueStr(colName).ToBool();
        }

        /// <summary>
        /// A DataRow extension method that converts the @this to the entities.
        /// </summary>
        /// <typeparam name="T">Generic type parameter.</typeparam>
        /// <param name="this">The @this to act on.</param>
        /// <returns>@this as a T.</returns>
        public static T ToEntity<T>(this DataRow @this) where T : new()
        {
            Type type = typeof(T);
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance);

            var entity = new T();

            foreach (PropertyInfo property in properties)
            {
                if (@this.Table.Columns.Contains(property.Name))
                {
                    Type valueType = property.PropertyType;
                    property.SetValue(entity, @this[property.Name].To(valueType), null);
                }
            }
            foreach (FieldInfo field in fields)
            {
                if (@this.Table.Columns.Contains(field.Name))
                {
                    Type valueType = field.FieldType;
                    field.SetValue(entity, @this[field.Name].To(valueType));
                }
            }
            return entity;
        }
    }
}
