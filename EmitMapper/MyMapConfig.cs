﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XQ.Extension.EmitMapper.MappingConfiguration;
using XQ.Extension.EmitMapper.XAttribute;

namespace XQ.Extension.EmitMapper
{
	public class MyMapConfig : DefaultMapConfig
	{
		// Token: 0x06000010 RID: 16 RVA: 0x000021DF File Offset: 0x000003DF
		public override IMappingOperation[] GetMappingOperations(Type from, Type to)
		{
			base.IgnoreMembers(from, to, this.GetIgnoreFields(from).Concat(this.GetIgnoreFields(to)).ToArray<string>());
			return base.GetMappingOperations(from, to);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x0000220C File Offset: 0x0000040C
		private IEnumerable<string> GetIgnoreFields(Type type)
		{
			return (from f in type.GetFields()
					where f.GetCustomAttributes(typeof(IgnoreMergeAttribute), false).Length != 0
					select f.Name).Concat(from p in type.GetProperties()
										  where p.GetCustomAttributes(typeof(IgnoreMergeAttribute), false).Length != 0
										  select p.Name);
		}
	}
}
