﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension.EmitMapper
{
    public interface IMappingConfigurator
    {
        IMappingOperation[] GetMappingOperations(Type from, Type to);

        IRootMappingOperation GetRootMappingOperation(Type from, Type to);

        string GetConfigurationName();

        StaticConvertersManager GetStaticConvertersManager();
    }
}
