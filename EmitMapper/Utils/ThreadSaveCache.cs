﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension.EmitMapper.Utils
{
    internal class ThreadSaveCache
    {
        private Dictionary<string, object> _cache = new Dictionary<string, object>();

        public T Get<T>(string key, Func<object> getter)
        {
            lock (_cache)
            {
                if (!_cache.TryGetValue(key, out var value))
                {
                    value = getter();
                    _cache[key] = value;
                }

                return (T)value;
            }
        }
    }
}
