﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension.EmitMapper.Mappers
{
    internal abstract class CustomMapperImpl : ObjectsMapperBaseImpl
    {
        public CustomMapperImpl(
            ObjectMapperManager mapperMannager,
            Type TypeFrom,
            Type TypeTo,
            IMappingConfigurator mappingConfigurator,
            object[] storedObjects)
        {
            Initialize(mapperMannager, TypeFrom, TypeTo, mappingConfigurator, storedObjects);
        }
    }
}