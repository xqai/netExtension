﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension.EmitMapper.AST.Interfaces
{
    interface IAstNode
    {
        void Compile(CompilationContext context);
    }
}
