﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension.EmitMapper.AST.Interfaces
{
    interface IAstStackItem : IAstNode
    {
        Type itemType { get; }
    }
}
