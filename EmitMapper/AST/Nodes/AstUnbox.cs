﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using XQ.Extension.EmitMapper.AST.Interfaces;

namespace XQ.Extension.EmitMapper.AST.Nodes
{
    class AstUnbox : IAstValue
    {
        public Type unboxedType;
        public IAstRef refObj;

        public Type itemType
        {
            get { return unboxedType; }
        }

        public void Compile(CompilationContext context)
        {
            refObj.Compile(context);
            context.Emit(OpCodes.Unbox_Any, unboxedType);
        }
    }
}
