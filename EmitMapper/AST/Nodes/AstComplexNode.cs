﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XQ.Extension.EmitMapper.AST.Interfaces;

namespace XQ.Extension.EmitMapper.AST.Nodes
{
    class AstComplexNode : IAstNode
    {
        public List<IAstNode> nodes = new List<IAstNode>();

        public void Compile(CompilationContext context)
        {
            foreach (IAstNode node in nodes)
            {
                if (node != null)
                {
                    node.Compile(context);
                }
            }
        }
    }
}
