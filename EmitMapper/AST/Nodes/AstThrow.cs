﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using XQ.Extension.EmitMapper.AST.Interfaces;

namespace XQ.Extension.EmitMapper.AST.Nodes
{
    class AstThrow : IAstNode
    {
        public IAstRef exception;

        public void Compile(CompilationContext context)
        {
            exception.Compile(context);
            context.Emit(OpCodes.Throw);
        }
    }
}