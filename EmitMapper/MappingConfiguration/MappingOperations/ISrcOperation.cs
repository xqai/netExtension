﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace XQ.Extension.EmitMapper.MappingConfiguration.MappingOperations
{
	public interface ISrcOperation : IMappingOperation
	{
		MemberDescriptor Source { get; set; }
	}
}
