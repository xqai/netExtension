﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XQ.Extension.EmitMapper.MappingConfiguration.MappingOperations
{
    interface IComplexOperation : IMappingOperation
    {
        List<IMappingOperation> Operations { get; set; }
    }
}
